package com.automation.chrome.contestants;

import java.io.File;

public class Constants {
    public static final String SEPARATOR = File.separator;
    public static final String REPORT_DIRECTORY = new File("").getAbsoluteFile() + SEPARATOR + "test-output";
    public static final String EXTENT_REPORT_PATH = REPORT_DIRECTORY + SEPARATOR + "ExtentReports" + SEPARATOR + "index.html";

}
