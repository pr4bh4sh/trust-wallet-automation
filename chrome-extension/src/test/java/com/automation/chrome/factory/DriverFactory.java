package com.automation.chrome.factory;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.io.File;


public class DriverFactory {

    static private WebDriver driver;

    /**
     * @return - Returns driver instance
     */
    public static WebDriver getDriver() {
        ChromeOptions options = new ChromeOptions();
        String extensionPath = new File("").getAbsoluteFile() + File.separator + "app" + File.separator + "trustwallet.crx";
        options.addExtensions(new File(extensionPath));
        driver = new ChromeDriver(options);
        return driver;
    }


    /**
     * Kills driver session
     */
    public static void quitDriver() {
        if (driver != null) {
            driver.quit();
        }
    }

    public static void closeDriver() {
        if (driver != null) {
            driver.close();
        }
    }
}
