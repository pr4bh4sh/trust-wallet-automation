package com.automation.chrome.listeners;

import com.automation.chrome.factory.DriverFactory;
import com.automation.chrome.report.ExtentReporterNG;
import com.automation.chrome.tests.BaseTest;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import java.io.IOException;

public class ReportListener extends BaseTest implements ITestListener {


    ExtentReports extent = ExtentReporterNG.getReporter();
    ExtentTest test;
    ThreadLocal<ExtentTest> extentTest = new ThreadLocal<>();


    @Override
    public void onTestStart(ITestResult result) {
        test = extent.createTest(result.getMethod().getMethodName());
        extentTest.set(test);

    }

    @Override
    public void onTestSuccess(ITestResult result) {
        //test.pass("Test Passed");
        extentTest.get().pass("Test Passed");

    }

    @Override
    public void onTestFailure(ITestResult result) {
        //test.fail(result.getThrowable());
        extentTest.get().fail(result.getThrowable());

        String filePath = null;
        try {
            filePath = getScreenshot(result.getMethod().getMethodName(), DriverFactory.getDriver());
        } catch (IOException e) {
            e.printStackTrace();
        }
        extentTest.get().addScreenCaptureFromPath(filePath, result.getMethod().getMethodName());
    }


    @Override
    public void onFinish(ITestContext context) {
        extent.flush();
    }
}
