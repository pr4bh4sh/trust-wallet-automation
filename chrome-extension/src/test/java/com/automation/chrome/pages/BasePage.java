package com.automation.chrome.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;

public class BasePage {
    protected Duration defaultTimeout = Duration.ofMillis(5000);
    WebDriver driver;

    public BasePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    protected void waitForWebElement(List<WebElement> elements, Duration timeInMilliSeconds) {
        waitForElement(elements, timeInMilliSeconds);
    }

    protected void waitForWebElement(List<WebElement> elements) {
        waitForElement(elements, defaultTimeout);
    }

    private void waitForElement(List<WebElement> elements, Duration defaultTimeout) {
        WebDriverWait wait = new WebDriverWait(driver, defaultTimeout);
        wait.pollingEvery(Duration.ofMillis(500));
        for (WebElement element : elements) {
            wait.until(ExpectedConditions.visibilityOf(element));
        }
    }

    protected void waitForWebElementBy(By by, Duration timeInMilliSeconds) {
        WebDriverWait wait = new WebDriverWait(driver, timeInMilliSeconds);
        wait.pollingEvery(Duration.ofMillis(500));
        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(by));
    }

    protected void navigateBack() {
        driver.navigate().back();
    }

    protected static boolean switchStatus(WebElement element) {
        return Boolean.parseBoolean(element.getAttribute("aria-checked"));
    }

}
