package com.automation.chrome.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import java.util.Arrays;

public class EnterSecretPhrasePage extends BasePage {
    @FindBy(css = "h2[data-testid='onboarding-step-title']")
    private WebElement titleTextElement;

    @FindBy(xpath = "//button[text()='Next']")
    private WebElement nextButtonElement;

    public EnterSecretPhrasePage(WebDriver driver) {
        super(driver);
    }

    public EnterSecretPhrasePage await() {
        waitForWebElement(Arrays.asList(titleTextElement), defaultTimeout);
        return this;
    }

    // region Action
    public void enterSecretPhrase(String passphrase) {
        String[] phrases = passphrase.split(" ");
        validateSecretPhrase(phrases);
        String phraseInputCssLocator = "input[placeholder='Word #%d']";

        for (int index = 0; index < phrases.length; index++) {
            String locator = String.format(phraseInputCssLocator, index + 1);
            String word = phrases[index];
            WebElement element = driver.findElement(By.cssSelector(locator));
            element.sendKeys(word);
        }
    }
    public ImproveTrustWalletPage clickNext() {
        nextButtonElement.click();
        return new ImproveTrustWalletPage(driver);
    }

    // endregion

    // region Verification

    private void validateSecretPhrase(String[] strings) {
        Assert.assertTrue(strings.length == 12 || strings.length == 24);
    }

    public void verifyNextButtonIsDisabled() {
        Assert.assertEquals(nextButtonElement.getAttribute("disabled"), "true");
    }

    public void verifyNextButtonIsEnabled() {
        Assert.assertNotEquals(nextButtonElement.getAttribute("disabled"), "true");
    }

    // endregion
}
