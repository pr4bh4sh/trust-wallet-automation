package com.automation.chrome.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.time.Duration;
import java.util.Arrays;

public class ImproveTrustWalletPage extends BasePage {
    @FindBy(css = "h2[data-testid='onboarding-step-title']")
    private WebElement titleTextElement;
    @FindBy(xpath = "//p[text() = 'No thanks']")
    WebElement noThanksElement;

    public ImproveTrustWalletPage(WebDriver driver) {
        super(driver);
    }

    // region Action
    public WalletImportedPage clickNoThanks() {
        noThanksElement.click();
        return new WalletImportedPage(driver);
    }

    public ImproveTrustWalletPage await() {
        waitForWebElement(Arrays.asList(titleTextElement, noThanksElement), Duration.ofMillis(10000));
        return this;
    }
    //endregion

}
