package com.automation.chrome.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.Arrays;

public class WalletImportedPage extends BasePage {

    @FindBy(css = "button[role='switch']")
    private WebElement defaultWalletSwitchElement;
    @FindBy(xpath = "//button[text() = 'Open wallet']")
    private WebElement openWalletButtonElement;

    public WalletImportedPage(WebDriver driver) {
        super(driver);
    }

    public WalletImportedPage await() {
        waitForWebElement(Arrays.asList(defaultWalletSwitchElement, openWalletButtonElement), defaultTimeout);
        return this;
    }

    public void disableDefaultWallet(){
        defaultWalletSwitchElement.click();
    }

    public WalletHomePage openWallet(){
        openWalletButtonElement.click();
        return new WalletHomePage(driver);
    }


}
