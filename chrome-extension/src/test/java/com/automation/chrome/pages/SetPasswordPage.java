package com.automation.chrome.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import java.util.Arrays;

public class SetPasswordPage extends BasePage {

    // region Locators
    @FindBy(css = "h2[data-testid='onboarding-step-title']:visible")
    private WebElement titleTextElement;

    @FindBy(css = "p.title-text.text-textPrimary:visible")
    private WebElement createNewWalletElement;

    @FindBy(css = "p.title-text.text-textSecondary:visible")
    private WebElement importWalletElement;

    @FindBy(xpath = "(//p/following-sibling::div//input)[1]")
    private WebElement newPasswordInputElement;

    @FindBy(xpath = "(//p/following-sibling::div//input)[2]")
    private WebElement confirmPasswordInputElement;

    @FindBy(css = "input[type='checkbox']")
    private WebElement checkboxElement;
    @FindBy(xpath = "//button[text()='Next']")
    private WebElement nextButtonElement;

    // endregion

    public SetPasswordPage await() {
        waitForWebElement(Arrays.asList(titleTextElement), defaultTimeout);
        return this;
    }

    public SetPasswordPage(WebDriver driver) {
        super(driver);
    }

    public void setNewPassword(String password) {
        newPasswordInputElement.sendKeys(password);
    }

    // region Action
    public void confirmPassword(String password) {
        confirmPasswordInputElement.sendKeys(password);
    }

    public void setCheckBoxStatus(boolean status) {
        boolean currentStatus = checkboxElement.isSelected();
        if (currentStatus == status) return;

        checkboxElement.click();
    }

    public EnterSecretPhrasePage clickNext() {
        nextButtonElement.click();
        return new EnterSecretPhrasePage(driver);
    }

    // endregion

    // region Verification
    public void verifyNextButtonIsDisabled() {
        Assert.assertEquals(nextButtonElement.getAttribute("disabled"), "true");
    }

    public void verifyNextButtonIsEnabled() {
        Assert.assertNotEquals(nextButtonElement.getAttribute("disabled"), "true");
    }

    // endregion

}
