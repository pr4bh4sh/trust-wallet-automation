package com.automation.chrome.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import java.time.Duration;
import java.util.Arrays;

public class SearchPage extends BasePage {

    @FindBy(xpath = "//p[text()='ALGO']/following-sibling::div/div/button")
    private WebElement algoButton;

    @FindBy(css = "input[placeholder = 'Token name or contract address']")
    private WebElement searchInputElement;

    public SearchPage(WebDriver driver) {
        super(driver);
    }

    public void findCryptoByName(String name) {
        waitForWebElement(Arrays.asList(searchInputElement));
        searchInputElement.sendKeys(name);
    }

    public void verifyCryptoInSearchResult(String name) {
        String locatorString = "//div/p[text() = '%s']";
        String xpathLocator = String.format(locatorString, name.toUpperCase());
        waitForWebElementBy(By.xpath(xpathLocator), Duration.ofMillis(10000));
    }

    public void setCryptoVisibilityByName(String name, boolean visibility) {
        String locatorString = "//p[text() = '%s']/../../../div/button[@role='switch']";
        String xpathLocator = String.format(locatorString, name);
        WebElement element = driver.findElement(By.xpath(xpathLocator));

        if (switchStatus(element) == visibility) return;

        element.click();
    }

    public void verifyCryptoVisibilityByName(String name, boolean visibility) {
        String locatorString = "//p[text() = '%s']/../../../div/button[@role='switch']";
        String xpathLocator = String.format(locatorString, name);
        WebElement element = driver.findElement(By.xpath(xpathLocator));

        Assert.assertEquals(switchStatus(element), visibility);
    }


//    public WalletHomePage presBack() {
//        navigateBack();
//        return new WalletHomePage(driver);
//    }
}
