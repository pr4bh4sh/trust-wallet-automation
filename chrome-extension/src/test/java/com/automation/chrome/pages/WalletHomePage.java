package com.automation.chrome.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class WalletHomePage extends BasePage {

    @FindBy(xpath = "//h3[text() = 'Tip 1 of 2']")
    WebElement toolTipTitleElement;
    @FindBy(xpath = "//button[text() = 'Got it']")
    WebElement gotItButtonElement;
    @FindBy(xpath = "//button[text() = 'I’m ready to use Trust Wallet']")
    WebElement readyToUseWalletButtonElement;

    @FindBy(xpath = "//div[@data-tooltip-content='Search']")
    private WebElement searchButtonElement;

    @FindBy(css = "div[role='tabpanel'] div[role='button'] .flex-grow p")
    private List<WebElement> cryptoElementTextList;

    public WalletHomePage(WebDriver driver) {
        super(driver);
    }

    public WalletHomePage await() {
        waitForWebElement(Arrays.asList(gotItButtonElement), defaultTimeout);
        return this;
    }

    public void dismissToolTip() {
        clickGotItButton();
        clickReadyToUseWalletButton();
    }

    public SearchPage openSearch() {
        searchButtonElement.click();
        return new SearchPage(driver);
    }

    private void clickGotItButton() {
        gotItButtonElement.click();
    }

    private void clickReadyToUseWalletButton() {
        waitForWebElement(Arrays.asList(readyToUseWalletButtonElement), defaultTimeout);
        readyToUseWalletButtonElement.click();
    }

    public void verifyWalletHomePageButtonTab() {

        String tabTitle = "Home Earn History Settings";
        List<String> titles = Arrays.asList(tabTitle.split(" "));

        List<WebElement> locators = new ArrayList<>();
        String locatorString = "//small[text() = '%s']";
        for (String title : titles) {
            String locator = String.format(locatorString, title);
            WebElement element = driver.findElement(By.xpath(locator));
            locators.add(element);
        }

        waitForWebElement(locators, defaultTimeout);
    }

    public void validateCryptoAvailabilityByName(String cryptoName, Boolean available) {
        List<String> cryptoNameList = new ArrayList<>();

        for (WebElement element : cryptoElementTextList) {
            cryptoNameList.add(element.getText());
        }

        Assert.assertEquals(available, cryptoNameList.contains(cryptoName));
    }
}
