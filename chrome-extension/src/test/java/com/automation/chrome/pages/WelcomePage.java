package com.automation.chrome.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import java.util.Arrays;
import java.util.List;

public class WelcomePage extends BasePage {

    @FindBy(css = "h2[data-testid='onboarding-step-title']")
    WebElement titleTextElement;

    @FindBy(xpath = "//*[text()='Create New Wallet']")
    private WebElement createNewWalletElement;

    @FindBy(xpath = "//p[text()='Import or recover wallet']")
    private WebElement importOrRecoverWalletElement;



    public WelcomePage(WebDriver driver) {
        super(driver);
    }

    public WelcomePage await() {
        List<WebElement> elements = Arrays.asList(titleTextElement);
        waitForWebElement(elements, defaultTimeout);
        return this;
    }

    public SetPasswordPage clickImportWallet() {
        importOrRecoverWalletElement.click();
        return new SetPasswordPage(driver);
    }

    public void validateTitle() {
        Assert.assertEquals(driver.getTitle(), "Trust Wallet");
    }
}
