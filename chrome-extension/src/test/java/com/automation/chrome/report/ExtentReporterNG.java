package com.automation.chrome.report;

import com.automation.chrome.contestants.Constants;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;

public class ExtentReporterNG {

    public static ExtentReports getReporter() {
        ExtentSparkReporter reporter = new ExtentSparkReporter(Constants.EXTENT_REPORT_PATH);
        reporter.config().setReportName("Trust Wallet");
        reporter.config().setDocumentTitle("Test Results");
        ExtentReports extent = new ExtentReports();
        extent.attachReporter(reporter);
        return extent;
    }


}
