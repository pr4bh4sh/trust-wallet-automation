package com.automation.chrome.tests;

import com.automation.chrome.contestants.Constants;
import com.automation.chrome.factory.DriverFactory;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.*;

import java.io.File;
import java.io.IOException;
import java.time.Duration;

public class BaseTest {
    protected WebDriver driver;

    @BeforeSuite
    static void setupClass() {
        WebDriverManager.chromedriver().setup();
    }

    @BeforeMethod
    public void setup() {
        driver = DriverFactory.getDriver();
        driver.get("https://google.com");
//      wait for extension tab to open
        new WebDriverWait(driver, Duration.ofMillis(20000)).until(ExpectedConditions.numberOfWindowsToBe(2));
        String originalWindow = driver.getWindowHandle();
        for (String windowHandle : driver.getWindowHandles()) {
            if (!originalWindow.contentEquals(windowHandle)) {
                driver.switchTo().window(windowHandle);
                break;
            }
        }
    }

//    @AfterTest
//    public void teatDown() {
//        DriverFactory.closeDriver();
//    }

    //    @AfterSuite
    @AfterMethod
    public void tearDownClass() {
        DriverFactory.quitDriver();
    }


    public String getScreenshot(String testCaseName, WebDriver driver) throws IOException {
        TakesScreenshot screenshot = (TakesScreenshot) driver;
        File srcfile = screenshot.getScreenshotAs(OutputType.FILE);
        String path = Constants.REPORT_DIRECTORY + Constants.SEPARATOR + testCaseName + ".png";
        File destfile = new File(path);
        FileUtils.copyFile(srcfile, destfile);
        return path;
    }
}
