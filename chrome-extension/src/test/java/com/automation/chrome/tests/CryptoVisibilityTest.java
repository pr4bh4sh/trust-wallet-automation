package com.automation.chrome.tests;

import com.automation.chrome.pages.*;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class CryptoVisibilityTest extends BaseTest {

    @DataProvider(name = "testData")
    public Object[][] testData() {
        return new Object[][]{
                {"ALGO", false},
                {"ETC", true},
        };
    }

    @Test(dataProvider = "testData")
    public void visibilityOfCryptoInWallet(String cryptoName, boolean visibility) {
        WalletHomePage walletHomePage = openWallet();
        SearchPage searchPage = walletHomePage.openSearch();
        searchPage.findCryptoByName(cryptoName);
        searchPage.verifyCryptoInSearchResult(cryptoName);
        searchPage.setCryptoVisibilityByName(cryptoName, visibility);
        searchPage.verifyCryptoVisibilityByName(cryptoName, visibility);
        driver.navigate().back();
        walletHomePage.validateCryptoAvailabilityByName(cryptoName, visibility);
    }

    private WalletHomePage openWallet() {
        WelcomePage welcomePage = new WelcomePage(driver).await();
        SetPasswordPage passwordPage = welcomePage.clickImportWallet();

        String password = "Roulette-copious3-unwitting";
        passwordPage.setNewPassword(password);
        passwordPage.confirmPassword(password);
        passwordPage.setCheckBoxStatus(true);

        EnterSecretPhrasePage enterSecretPhrasePage = passwordPage.clickNext();
        enterSecretPhrasePage.await();

        String passphrase = "place need amount action giggle oil stadium april hour disorder vicious cement";
        enterSecretPhrasePage.enterSecretPhrase(passphrase);
        ImproveTrustWalletPage improveTrustWalletPage = enterSecretPhrasePage.clickNext();
        improveTrustWalletPage.await();
        WalletImportedPage walletImportedPage = improveTrustWalletPage.clickNoThanks();
        walletImportedPage.await();
        walletImportedPage.disableDefaultWallet();
        WalletHomePage walletHomePage = walletImportedPage.openWallet();
        walletHomePage.await();
        walletHomePage.dismissToolTip();
        return walletHomePage;
    }
}
