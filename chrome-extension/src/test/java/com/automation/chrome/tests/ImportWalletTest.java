package com.automation.chrome.tests;

import com.automation.chrome.pages.*;
import org.testng.annotations.Test;

public class ImportWalletTest extends BaseTest {

    @Test
    public void importWallet() {
        WelcomePage welcomePage = new WelcomePage(driver).await();
        welcomePage.validateTitle();
        SetPasswordPage passwordPage = welcomePage.clickImportWallet();
        passwordPage.verifyNextButtonIsDisabled();

        String password = "Roulette-copious3-unwitting";
        passwordPage.setNewPassword(password);
        passwordPage.confirmPassword(password);
        passwordPage.setCheckBoxStatus(true);
        passwordPage.verifyNextButtonIsEnabled();

        EnterSecretPhrasePage enterSecretPhrasePage = passwordPage.clickNext();
        enterSecretPhrasePage.await();

        String passphrase = "place need amount action giggle oil stadium april hour disorder vicious cement";
        enterSecretPhrasePage.enterSecretPhrase(passphrase);
        ImproveTrustWalletPage improveTrustWalletPage = enterSecretPhrasePage.clickNext();
        improveTrustWalletPage.await();
        WalletImportedPage walletImportedPage = improveTrustWalletPage.clickNoThanks();
        walletImportedPage.await();
        walletImportedPage.disableDefaultWallet();
        WalletHomePage walletHomePage = walletImportedPage.openWallet();
        walletHomePage.await();
        walletHomePage.dismissToolTip();
        walletHomePage.verifyWalletHomePageButtonTab();
    }
}
