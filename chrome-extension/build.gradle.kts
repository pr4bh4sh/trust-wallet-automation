plugins {
    id("java")
}

group = "org.automation.chrome"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    testImplementation("org.seleniumhq.selenium:selenium-java:4.9.1")
    testImplementation("io.github.bonigarcia:webdrivermanager:5.6.3")
    testImplementation("com.aventstack:extentreports:5.1.1")

    testImplementation("org.testng:testng:7.9.0")

}

tasks.test {
    useTestNG() {
        suites("./src/test/resources/testsuite/testSuite.xml")
        outputDirectory = file("test-output")
    }
}