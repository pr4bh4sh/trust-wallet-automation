# TrustWallet Automation

Link to Test case document [Trust-wallet-test-case.xlsx](Trust-wallet-test-case.xlsx)

## Instroduction

## Pre condition

1. Node.js is installed on the machine and npm in available in command line
2. Chrome browser is installed on machine
3. Android sdk is setup on machine
3. One Android emulator is running
4. Clone the project and navigate to project root
    ```bash
   git clone git@gitlab.com:pr4bh4sh/trust-wallet-automation.git && trust-wallet-automation
   ```

### Steps for running test on mobile:

1. Install Appium using below command
    ```bash
    npm install -g appium
    appium driver install uiautomator2 
    ```
1. Launch Appium server in a terminal window by executing below command
    ```bash
    appium 
    ```
1. Run below gradle command to run the test on mobile test
   ```bash
   ./gradlew mobile-app:test
   ```
   Additionally, you can run it form intellij gradle plugin
   ![img.png](img.png)


1. Reports are generated at `test-output' and `mobile-app/build/reports/tests/test` directory
2. To debug/inspect the android UI during test
    1. Put break point at the place you want
    2. Run below command on terminal
       ```bash
       open -n -a /Applications/Google\ Chrome.app/Contents/MacOS/Google\ Chrome "https://inspector.appiumpro.com" --args --user-data-dir="/tmp/chrome_dev_test" --disable-web-security
       ```
    3. Set the field values as below screenshot and click on "Attach to session"
       ![img_2.png](img_2.png)
    4. You should see inspector page as below ![img_3.png](img_3.png)

### Steps for running test on chrome:

1. Run below gradle command to run the test on mobile test
   ```bash
   ./gradlew chrome-extension:test
   ```

1. Additionally, you can run it form intellij gradle plugin![img_1.png](img_1.png)

1. Reports are generated at `test-output' and `mobile-app/build/reports/tests/test` directory
