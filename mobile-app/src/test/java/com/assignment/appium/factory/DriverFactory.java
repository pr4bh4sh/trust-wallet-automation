package com.assignment.appium.factory;


import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.AutomationName;
import io.appium.java_client.remote.MobileCapabilityType;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.UUID;

public class DriverFactory {

    static private AndroidDriver appiumDriver;

    /**
     * @return - Returns driver instance
     */
    public static AndroidDriver getAndroidDriver() {
        if (appiumDriver != null) {
            return appiumDriver;
        } else {
            String package_name = "com.wallet.crypto.trustapp";
            DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
//      desiredCapabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "Android_device");
            desiredCapabilities.setCapability(MobileCapabilityType.APP,
                    new File("").getAbsoluteFile() + File.separator + "app" + File.separator
                            + "Trust-Wallet.apk");
            desiredCapabilities
                    .setCapability(AndroidMobileCapabilityType.APP_PACKAGE, package_name);
            desiredCapabilities.setCapability(AndroidMobileCapabilityType.NO_SIGN, true);
            desiredCapabilities.setCapability(MobileCapabilityType.NO_RESET, false);
            desiredCapabilities.setCapability(MobileCapabilityType.FULL_RESET, false);
            desiredCapabilities.setCapability("newCommandTimeout", 7200);
            desiredCapabilities.setCapability(AndroidMobileCapabilityType.UNICODE_KEYBOARD, true);
            desiredCapabilities.setCapability(AndroidMobileCapabilityType.RESET_KEYBOARD, true);
            desiredCapabilities.setCapability(AndroidMobileCapabilityType.AUTO_GRANT_PERMISSIONS, true);
            desiredCapabilities
                    .setCapability(MobileCapabilityType.AUTOMATION_NAME, AutomationName.ANDROID_UIAUTOMATOR2);
            try {
                appiumDriver = new AndroidDriver(new URL("http://0.0.0.0:4723"),
                        desiredCapabilities);

                return appiumDriver;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    /**
     * @return - Returns relative path with result dir
     */
    public static String getScreenShot() {
        try {
            System.out.println("Adding screen shot");
            File screenshot = appiumDriver.getScreenshotAs(OutputType.FILE);
            String path = "screenshots/" + UUID.randomUUID() + "" + ".png";
            System.out.println(path);
            FileUtils.copyFile(screenshot, new File(System.getProperty("user.dir") + "/reports/" + path));
            return path;
        } catch (Exception e) {
            System.out.println("Screen shot failed");
            System.out.println(e.getMessage());
        }
        return null;
    }

    /**
     * Kills driver session
     */
    public static void quitDriver() {
        if (appiumDriver != null) {
            appiumDriver.quit();
        }
    }
}
