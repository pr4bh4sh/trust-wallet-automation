package com.assignment.appium.test;

import com.assignment.appium.factory.DriverFactory;
import com.assignment.appium.pages.*;
import io.appium.java_client.android.AndroidDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.List;


public class CreateWalletTest extends BaseTest {

    @Test
    public void createWallet() {
        WelcomePage welcomePage = new WelcomePage(driver).await();
        CreateWalletPage createWalletPage = welcomePage.getStartedWithAccountCreation();
        BackUpSecretPage backUpSecretPage = createWalletPage.await().clickCreateNewWallet();
        SecretPhraseCheckPage secretPhraseCheckPage = backUpSecretPage.await().clickBackUpManuallyElement();
        secretPhraseCheckPage.await().checkAllCheckbox();
        SecretPhrasePage secretPhrasePage = secretPhraseCheckPage.clickContinue();

        List<String> secretPhrase = secretPhrasePage.getSecretPhrase();
        ConfirmSecretPhrasePage confirmSecretPhrasePage = secretPhrasePage.clickContinue();
        confirmSecretPhrasePage.await().confirmSecretPhrase(secretPhrase);
        CreatePassCodePage createPassCodePage = confirmSecretPhrasePage.clickContinue();

        String passcode = "123456";
        ConfirmPassCodePage confirmPassCodePage = createPassCodePage.await().setPassCode(passcode);

        HomePage homePage = confirmPassCodePage.confirmPassCode(passcode);
        homePage.await();
    }
}
