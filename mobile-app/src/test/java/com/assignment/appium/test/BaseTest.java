package com.assignment.appium.test;

import com.assignment.appium.contestants.Constants;
import com.assignment.appium.factory.DriverFactory;
import io.appium.java_client.android.AndroidDriver;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.*;

import java.io.File;
import java.io.IOException;
import java.time.Duration;

public class BaseTest {
    protected AndroidDriver driver;

    @BeforeClass
    public void setUp() {
        driver = DriverFactory.getAndroidDriver();
    }

    @AfterClass
    public void tearDown() {
        DriverFactory.quitDriver();
    }


    public String getScreenshot(String testCaseName, WebDriver driver) throws IOException {
        File srcfile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        String path = Constants.REPORT_DIRECTORY + Constants.SEPARATOR + testCaseName + ".png";
        File destfile = new File(path);
        FileUtils.copyFile(srcfile, destfile);
        return path;
    }
}
