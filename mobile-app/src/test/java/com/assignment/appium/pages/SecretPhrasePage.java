package com.assignment.appium.pages;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SecretPhrasePage extends BasePage {

    @AndroidFindBy(uiAutomator = "new UiSelector().textContains(\". \")")
    private List<WebElement> secretPhraseTexts;
    @AndroidFindBy(uiAutomator = "text(\"Secret phrase\")")
    private WebElement pageID;
    @AndroidFindBy(uiAutomator = "text(\"Continue\")")
    private WebElement continueButtonElement;
    @AndroidFindBy(uiAutomator = "text(\"Copy to Clipboard\")")
    private WebElement copyToClipBoard;


    public SecretPhrasePage(AndroidDriver driver) {
        super(driver);
    }

    public SecretPhrasePage await() {
        waitForWebElement(pageID, defaultTimeout);
        waitForWebElement(copyToClipBoard, defaultTimeout);
        return this;
    }

    public List<String> getSecretPhrase() {
        List<String> words = new ArrayList<String>(Collections.nCopies(12, ""));
        ;

        for (WebElement textElement : secretPhraseTexts) {
            String text = textElement.getText();
            if (!text.contains(". ")) continue;
            int index = Integer.parseInt(text.split(". ")[0]) - 1;
            String word = text.split(". ")[1];
            words.set(index, word);
        }
        return words;
    }

    public ConfirmSecretPhrasePage clickContinue() {
        continueButtonElement.click();
        return new ConfirmSecretPhrasePage(driver);
    }
}
