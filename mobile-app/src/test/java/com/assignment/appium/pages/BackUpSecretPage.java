package com.assignment.appium.pages;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.WebElement;

public class BackUpSecretPage extends BasePage {

    @AndroidFindBy(uiAutomator = "text(\"Back up secret phrase\")")
    private WebElement pageID;
    @AndroidFindBy(uiAutomator = "text(\"Back up manually\")")
    private WebElement backUpManuallyElement;
    @AndroidFindBy(uiAutomator = "text(\"Back up to Google Drive\")")
    private WebElement backUpToGoogleDriveElement;


    public BackUpSecretPage(AndroidDriver driver) {
        super(driver);
    }

    public BackUpSecretPage await() {
        waitForWebElement(pageID, defaultTimeout);
        waitForWebElement(backUpManuallyElement, defaultTimeout);
        return this;
    }

    public SecretPhraseCheckPage clickBackUpManuallyElement() {
        backUpManuallyElement.click();
        return new SecretPhraseCheckPage(driver);

    }
}
