package com.assignment.appium.pages;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.WebElement;

import java.time.Duration;

public class WelcomePage extends BasePage {

    @AndroidFindBy(uiAutomator = "text(\"Get Started\")")
    private WebElement getStartedButton;

    @AndroidFindBy(uiAutomator = "new UiSelector().textContains(\"By tapping\")")
    private WebElement pageID;

    public WelcomePage(AndroidDriver driver) {
        super(driver);
    }

    public WelcomePage await() {
        waitForWebElement(pageID, defaultTimeout);
        return this;
    }

    public CreateWalletPage getStartedWithAccountCreation() {
        getStartedButton.click();
        return new CreateWalletPage(driver);
    }
}

