package com.assignment.appium.pages;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.WebElement;

public class CreateWalletPage extends BasePage {

    @AndroidFindBy(uiAutomator = "text(\"Create new wallet\")")
    private WebElement createNewWalletElement;
    @AndroidFindBy(uiAutomator = "text(\"Add existing wallet\")")
    private WebElement addExistingWalletElement;


    public CreateWalletPage(AndroidDriver driver) {
        super(driver);
    }

    public CreateWalletPage await() {
        waitForWebElement(createNewWalletElement, defaultTimeout);
        waitForWebElement(addExistingWalletElement, defaultTimeout);
        return this;
    }

    public BackUpSecretPage clickCreateNewWallet() {
        createNewWalletElement.click();
        return new BackUpSecretPage(driver);
    }
}
