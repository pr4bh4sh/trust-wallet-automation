package com.assignment.appium.pages;

import io.appium.java_client.AppiumBy;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.WebElement;

import java.util.*;
import java.util.stream.Collectors;

public class ConfirmSecretPhrasePage extends BasePage {

    @AndroidFindBy(uiAutomator = "text(\"Confirm secret phrase\")")
    private WebElement pageID;
    @AndroidFindBy(uiAutomator = "text(\"Confirm\")")
    private WebElement confirmButtonElement;

    @AndroidFindBy(uiAutomator = "new UiSelector().textContains(\"Word #\")")
    private List<WebElement> expectedWordHeaderElement;

    public ConfirmSecretPhrasePage(AndroidDriver driver) {
        super(driver);
    }

    public ConfirmSecretPhrasePage await() {
        waitForWebElement(pageID, defaultTimeout);
        return this;
    }

    public void confirmSecretPhrase(List<String> secretPhraseTexts) {
        List<WebElement> availableOptionElements = driver.findElements(AppiumBy.className("android.widget.TextView"));
        List<WebElement> optionElements = availableOptionElements.stream().filter(word -> !word.getText().contains(" ") && !word.getText().contains("Confirm")).collect(Collectors.toList());
        ListIterator<WebElement> optionElementIterator = optionElements.listIterator();
        for (WebElement headerElement : expectedWordHeaderElement) {

            int index = Integer.parseInt(headerElement.getText().split(" #")[1]) - 1;
            String correctOption = secretPhraseTexts.get(index);

            while (optionElementIterator.hasNext()) {
                WebElement element = optionElementIterator.next();
                if (!element.getText().equals(correctOption)) continue;

                // Element found, Reset the iterator to the first item
                while (optionElementIterator.hasPrevious()) {
                    optionElementIterator.previous();
                }
                element.click();
                for (int i = 0; i <= 2 && optionElementIterator.hasNext(); i++) {
                    optionElementIterator.next(); // Use iterator.remove() to avoid ConcurrentModificationException
                    optionElementIterator.remove();
                }

                break;
            }
        }
    }

    public CreatePassCodePage clickContinue() {
        confirmButtonElement.click();
        return new CreatePassCodePage(driver);
    }
}
