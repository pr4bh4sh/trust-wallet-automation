package com.assignment.appium.pages;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.WebElement;

import java.time.Duration;

public class HomePage extends BasePage {
    @AndroidFindBy(uiAutomator = "text(\"Welcome aboard\")")
    private WebElement pageID;

    public HomePage(AndroidDriver driver) {
        super(driver);
    }

    public HomePage await() {
        waitForWebElement(pageID, Duration.ofMillis(5000));
        return this;
    }
}
