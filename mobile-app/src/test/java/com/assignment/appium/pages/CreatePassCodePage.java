package com.assignment.appium.pages;

import io.appium.java_client.AppiumBy;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.WebElement;

import java.time.Duration;

public class CreatePassCodePage extends BasePage {

    @AndroidFindBy(uiAutomator = "text(\"Create passcode\")")
    private WebElement pageID;

    public CreatePassCodePage(AndroidDriver driver) {
        super(driver);
    }

    public CreatePassCodePage await() {
        waitForWebElement(pageID, Duration.ofMillis(10000));
        return this;
    }

    public ConfirmPassCodePage setPassCode(String passcode) {
        for (char ch : passcode.toCharArray()) {
            WebElement element = driver.findElement(AppiumBy.androidUIAutomator("text(\"" + String.valueOf(ch) + "\")"));
            waitForWebElement(element, defaultTimeout);
            element.click();
        }
        return new ConfirmPassCodePage(driver);
    }
}

