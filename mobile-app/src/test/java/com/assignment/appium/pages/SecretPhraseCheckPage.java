package com.assignment.appium.pages;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.WebElement;

import java.util.List;

public class SecretPhraseCheckPage extends BasePage {

    @AndroidFindBy(className = "android.widget.TextView")
    private List<WebElement> textElements;
    @AndroidFindBy(uiAutomator = "text(\"This secret phrase is the master key to your wallet\")")
    private WebElement pageID;
    @AndroidFindBy(uiAutomator = "text(\"Continue\")")
    private WebElement continueButtonElement;


    public SecretPhraseCheckPage(AndroidDriver driver) {
        super(driver);
    }

    public SecretPhraseCheckPage await() {
        waitForWebElement(pageID, defaultTimeout);
        waitForWebElement(continueButtonElement, defaultTimeout);
        return this;
    }

    public void checkAllCheckbox() {
        for (int index = 2; index < textElements.size() - 1; index++) {
            WebElement element = textElements.get(index);
            waitForWebElement(element, defaultTimeout);
            element.click();
        }
    }

    public SecretPhrasePage clickContinue() {
        continueButtonElement.click();
        return new SecretPhrasePage(driver);
    }
}
