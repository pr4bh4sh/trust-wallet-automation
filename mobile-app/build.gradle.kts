plugins {
    id("java")
}

group = "org.automation.appium"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    testImplementation("org.testng:testng:7.9.0")
    testImplementation("org.seleniumhq.selenium:selenium-api:4.9.1")
    testImplementation("org.seleniumhq.selenium:selenium-support:4.9.1")

    testImplementation("org.apache.logging.log4j:log4j:2.22.1")
    testImplementation("io.appium:java-client:8.5.1")
    testImplementation("commons-io:commons-io:2.11.0")
    testImplementation("com.aventstack:extentreports:5.1.1")
}

tasks.test {
    useTestNG() {
        suites("./src/test/resources/testsuite/testSuite.xml")
        outputDirectory = file("test-output")
    }
}